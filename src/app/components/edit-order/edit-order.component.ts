import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Category } from 'src/app/models/Category';
import { Product } from 'src/app/models/Product';
import { Order } from 'src/app/models/Order';
import { OrderService } from 'src/app/services/order.service';
import { User } from 'src/app/models/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { userInfo } from 'os';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.scss']
})
export class EditOrderComponent implements OnInit {

  public categoryList: Category[] = [];
  public currentCategory : Category = new Category(0, "loading", []);

  currentUser: User;

  constructor(private orderService: OrderService, private authService: AuthenticationService) { 
   
  }

  ngOnInit() {

    this.orderService.categoryListSubject.subscribe((categories: Category[]) => {
      this.categoryList = categories;
    })

    this.authService.currentUserSubject.subscribe((user: User) => {
      this.currentUser = user;
    });

    this.authService.getCurrentUser();

    this.orderService.createOrder(this.currentUser);

    this.orderService.getAllProductWithCategory();
  }

  indexProdInf(product: Product, index: number) {
    if (this.currentCategory.products.indexOf(product) < index) {
      return true;
    }

    return false;
  }

  setCurrentCategory(event): void {
    this.currentCategory = event;
  }

}
