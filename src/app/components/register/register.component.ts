import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDatepickerModule, MatDatepicker } from '@angular/material';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { User } from 'src/app/models/User';
import { Address } from 'src/app/models/Address';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  /**
   * Variable pour le formulaire register
  */
  public registerForm: FormGroup;

  constructor(private router: Router, private authService: AuthenticationService) { }

  /**
    * Méthode d'initialisation
  */
  ngOnInit() {

    this.registerForm = new FormGroup({
      lastname: new FormControl('', [
        Validators.required
      ]),
      firstname: new FormControl('', [
        Validators.required
      ]),
      numero: new FormControl('', [
        Validators.required
      ]),
      street: new FormControl('', [
        Validators.required
      ]),
      city: new FormControl('', [
        Validators.required
      ]),
      zipCode: new FormControl('', [
        Validators.required
      ]),
      country: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      dateOfBirth: new FormControl('', [
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
    });

  }

  /**
   * Méthode de création de l'objet User pour l'enregistrement d'un compte
  */
  prepareRegisterObj(): User {

    let user: User = new User();
    user.lastname = this.registerForm.controls.lastname.value;
    user.firstname = this.registerForm.controls.firstname.value;

    var datePipe = new DatePipe('en-EN');
    var date = datePipe.transform(this.registerForm.controls.dateOfBirth.value, 'yyyy-MM-dd');
    user.dateOfBirth = date;

    user.mail = this.registerForm.controls.email.value;
    user.password = this.registerForm.controls.password.value;

    let address: Address = new Address();
    address.numero = +(this.registerForm.controls.numero.value);
    address.street = this.registerForm.controls.street.value;
    address.zipCode = this.registerForm.controls.zipCode.value;
    address.city = this.registerForm.controls.city.value;
    address.country = this.registerForm.controls.country.value;
    user.address = address;

    return user;

  }

  /**
   * Méthode pour s'enregistrer dans l'application
  */
  submitRegister() {
    const user = this.prepareRegisterObj();
    this.authService.createUser(user);
  }

  /**
  * Méthode pour rediriger sur la page Login
  */
  redirectToLogin(): void {
    this.router.navigate(['login']);
  }

  /**
  * Méthode pour rediriger sur la page Login
  */
  redirectToHome(): void {
    this.router.navigate(['']);
  }

}
