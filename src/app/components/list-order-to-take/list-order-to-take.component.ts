import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/models/Order';
import { OrderService } from 'src/app/services/order.service';
import { MatDialog } from '@angular/material';
import { ListOrderToGiveComponent } from '../list-order-to-give/list-order-to-give.component';
import { StatusOrderEnum } from 'src/app/enum/StatusOrderEnum';

@Component({
  selector: 'app-list-order-to-take',
  templateUrl: './list-order-to-take.component.html',
  styleUrls: ['./list-order-to-take.component.scss']
})
export class ListOrderToTakeComponent implements OnInit {

  displayedColumns: string[] = ['No. Order', 'Numero', 'Owner', 'Price', 'Action'];
  dataSource: Order[] = [];

  constructor(private orderService: OrderService, public dialog: MatDialog) { }

  ngOnInit() {
    this.orderService.allOrdersNotOwnedSubject.subscribe((orders: Order[]) => {
      this.dataSource = orders.filter((order: Order) => order.status == StatusOrderEnum.NotAssigned);
    })
    console.log(this.dataSource);
  }

  openDialog(order: Order): void {

    const dialogRef = this.dialog.open(ListOrderToGiveComponent, {width: '250px'});
    let instance = dialogRef.componentInstance;
    instance.orderOwner = order;
    instance.modal = true;
    console.log('dialogRef',dialogRef);
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
