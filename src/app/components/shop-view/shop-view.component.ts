import { Component, OnInit } from '@angular/core';
import { ShopInfoService } from 'src/app/services/shop-info.service';
import { OrderService } from 'src/app/services/order.service';
import { MatTreeNestedDataSource } from '@angular/material';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Category } from 'src/app/models/Category';
import { Product } from 'src/app/models/Product';
import { Order } from 'src/app/models/Order';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shop-view',
  templateUrl: './shop-view.component.html',
  styleUrls: ['./shop-view.component.scss']
})
export class ShopViewComponent implements OnInit {

  order: Order = new Order();
  orderSum: number = 0;

  constructor(private shopInfo: ShopInfoService, private orderService: OrderService) {
    this.order.categoryOrdered = [];
  }

  ngOnInit() {

    this.orderService.orderSubject.subscribe((order: Order) => {
      this.order = order;
      this.orderSum = this.calculSum(order);
    })

    this.orderService.emitOrderSubject();

  }

  hide() {
    this.shopInfo.hide();
  }

  submitOrder() {
    this.orderService.sendOrder();
  }

  calculSum(order: Order): number {
    let sum = 0;

    order.categoryOrdered.forEach((category: Category) => {
      category.products.forEach(product => {
        sum += product.priceMax;
      });
    });

    return sum;
  }

}
