import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/models/Order';
import { User } from 'src/app/models/User';
import { Product } from 'src/app/models/Product';
import { OrderService } from 'src/app/services/order.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-list-order-to-give',
  templateUrl: './list-order-to-give.component.html',
  styleUrls: ['./list-order-to-give.component.scss']
})
export class ListOrderToGiveComponent implements OnInit {

  @Input() orderOwner: Order;
  
  modal: boolean = false;

  productList: Product[] = [];

  displayedColumns: string[] = ['No. Product', 'Name', 'Quantity', 'Price'];
  dataSource: Product[];

  constructor(private orderService: OrderService, public dialogRef: MatDialogRef<ListOrderToGiveComponent>) { }

  ngOnInit() {
    this.calculSumProduct();
    this.makeProductList();
    this.dataSource = this.productList;
  }

  makeProductList() {
    this.productList = [];
    this.orderOwner.categoryOrdered.forEach(category => {
      category.products.forEach((product: Product) => {
        this.productList.push(product);
      });
    });
  }

  calculSumProduct() {
    this.orderOwner.categoryOrdered.forEach(category => {
      category.products.forEach((product: Product) => {
        product.priceMax = product.quantity * product.unitPrice;
      });
    });
  }

  cancelOrder() {
    this.orderService.cancelOrder(this.orderOwner.id);
  }

  closeModal() {
    this.dialogRef.close();
  }

  deliverOrder() {
    this.closeModal();
    this.orderService.takeOrderToDeliver(this.orderOwner);
  }

  closeOrder() {
    this.orderService.closeOrder(this.orderOwner);
  }

  isNotDelivererUser(): boolean {
    if (this.orderOwner.deliveryMan.id != +localStorage.getItem('userId')) {
      return true;
    }
    return false;
  }
}
