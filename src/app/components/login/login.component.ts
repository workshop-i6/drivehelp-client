import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /**
   * Variable pour le formulaire login
  */
  public loginForm: FormGroup;

  constructor(private router: Router, private snackBar: MatSnackBar, private authService: AuthenticationService) { }

  ngOnInit() {

    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
    });

  }

  /**
   * Méthode pour se connecter dans l'application
  */
  submitLogin(): void {
    if ((!this.loginForm.controls.email.hasError('email') && !this.loginForm.controls.email.hasError('required'))
      && (!this.loginForm.controls.password.hasError('minLength') && !this.loginForm.controls.password.hasError('required'))) {
      this.authService.login(this.loginForm.controls.email.value, this.loginForm.controls.password.value);
    } else {
      this.openSnackBar("Informations no completed !")
    }

  }

  /**
   * Méthode pour rediriger sur la page Register
  */
  redirectToRegister(): void {
    this.router.navigate(['register']);
  }
  
  /**
  * Méthode pour rediriger sur la page Login
  */
  redirectToHome(): void {
    this.router.navigate(['']);
  }

  /**
  * Méthode pour rediriger sur la page Principale
  */
  redirectToMain(): void {
    this.router.navigate(['action']);
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 2000,
    });
  }

}
