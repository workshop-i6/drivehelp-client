import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private snackBar: MatSnackBar, private router: Router) { }

  /**
     * Méthode d'initialisation
  */
  ngOnInit() {
  }

  /**
   * Méthode pour rediriger sur la page Login
  */
  redirectToLogin(): void {
    this.router.navigate(['login']);
  }

  /**
   * Méthode pour rediriger sur la page Login
  */
  redirectToRegister(): void {
    this.router.navigate(['register']);
  }

  /**
   * Méthode pour rediriger sur la page Home
  */
  redirectToHome(): void {
    this.router.navigate(['']);
  }

  /**
   * Méthode pour ouvrir un popup avec un message spécifique
  */
  openSnackBar(message: string) {
    this.snackBar.open(message, null, {
      duration: 2000,
    });
  }

  /**
   * Méthode pour rediriger sur la page Home
  */
 redirectToMain(): void {
  this.router.navigate(['action']);
}

}
