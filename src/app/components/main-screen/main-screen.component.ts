import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ShopInfoService } from 'src/app/services/shop-info.service';
import { OrderService } from 'src/app/services/order.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/User';
import { Order } from 'src/app/models/Order';
import { MatTabChangeEvent } from '@angular/material';
import { interval } from 'rxjs';

@Component({
  selector: 'app-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.scss']
})
export class MainScreenComponent implements OnInit {

  displayShopInfo: boolean;
  orderOwner: Order = null;
  orderOwnerCheck: boolean = false;
  currentUser: User = new User();

  allOrdersNotOwned: Order[] = [];

  orderAssigned: Order = null;

  constructor(private router: Router, public shopInfo: ShopInfoService, public orderService: OrderService, private authService: AuthenticationService) { }

  ngOnInit() {

    interval(10000).subscribe(() => {
      this.orderService.getAllOrders();
      this.orderService.getAllOrdersOwner();
    })

    this.shopInfo.displayShSubject.subscribe((res: boolean) => {
      this.displayShopInfo = res
    });

    this.orderService.orderOwnerSubject.subscribe((orderOwner: any) => {
      this.orderOwner = orderOwner;
    });

    this.shopInfo.emitDisplayShSubject();

    this.authService.currentUserSubject.subscribe((user: User) => {
      if(!this.orderOwnerCheck) {
        setTimeout(() => {
          this.currentUser = user;
          this.orderService.getAllOrdersOwner();
        }, 150);
        this.orderOwnerCheck = true;
      }
    });

    this.orderService.allOrdersNotOwnedSubject.subscribe((orders: Order[]) => {
      this.allOrdersNotOwned = orders;
    });

    this.orderService.getAllOrders();

    this.orderService.orderAssignedSubject.subscribe((order: Order) => {
      this.orderAssigned = order;
    })
    
  }

  /**
 * Méthode pour rediriger sur la page Login
*/
  redirectToHome(): void {
    this.router.navigate(['']);
  }
  
  onChangeTab(tabChangeEvent: MatTabChangeEvent) {
    if(tabChangeEvent.index == 1) {
      this.orderService.getAllOrders();
    } else {
      this.orderService.getAllOrdersOwner();
    }
  }

  currentUserIsDeliverMan(order: Order): boolean {
    if(order.deliveryMan != null) {
      if(order.deliveryMan.id == +localStorage.getItem('userId')) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

}
