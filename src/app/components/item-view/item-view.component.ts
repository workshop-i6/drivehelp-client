import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-item-view',
  templateUrl: './item-view.component.html',
  styleUrls: ['./item-view.component.scss']
})
export class ItemViewComponent implements OnInit {

  @Input() product: Product;

  constructor(private orderService: OrderService) { }

  ngOnInit() {

  }

  setQuantityProduct(quantity: number) {
    if (quantity < 0) {
      if (this.product.quantity != 0) {
        this.orderService.qantityProduct(this.product, quantity);
      }
    } else {
      this.orderService.qantityProduct(this.product, quantity);
    }
  }

}
