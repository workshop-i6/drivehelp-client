import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/User';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  /*
  ** Variable qui contient l'utilisateur connecté à l'application
  */
  public currentUser: User = null;

  constructor(private authService: AuthenticationService, private orderService: OrderService, private router: Router) { }

  /*
  ** Méthode d'initialisation
  */
  ngOnInit() {
    this.authService.currentUserSubject.subscribe((user: User) => {
      this.currentUser = user;
    });

   this.authService.getCurrentUser();
  }

  /*
  ** Méthode qui signale la déconnexion de l'utilisateur au service AuthenticationService
  */
  disconnect() {
    this.authService.disconnect();
    this.orderService.clearCategoryList();
    this.orderService.clearOrder();
    this.router.navigate(['login']);
  }

}
