import { Address } from './Address';

export class User {
    id: number;
    lastname: string;
    firstname: string;
    dateOfBirth: string;
    mail: string;
    password: string;
    address: Address;
    token: string;
}