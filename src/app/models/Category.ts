import { Product } from './Product';

export class Category {

    id: number;
    name: string;
    products: Product[]
    img: string;

    constructor(id: number, name: string, products?: Product[]) {
        this.id = id;
        this.name = name;
        this.products = products;

        switch (id) {
            case 1:
                this.img = "./../../../assets/img/cat/alimentaire/alimentaire.png"
                break;
            case 2:
                this.img = "./../../../assets/img/cat/hygiene/hygiene.png"
                break;
            case 10:
                this.img = "./../../../assets/img/cat/viande/viande.png"
                break;
            case 11:
                this.img = "./../../../assets/img/cat/legume/legume.png"
                break;
            case 12:
                this.img = "./../../../assets/img/cat/fruit/fruit.png"
                break;
            case 13:
                this.img = "./../../../assets/img/cat/fromage/fromage.png"
                break;
            case 14:
                this.img = "./../../../assets/img/cat/feculent/feculent.png"
                break;
            case 15:
                this.img = "./../../../assets/img/cat/entretien/entretien.png"
                break;
        }
    }
}