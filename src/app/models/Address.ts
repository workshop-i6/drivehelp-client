export class Address {
    id: number;
    numero: number;
    street: string;
    city: string;
    zipCode: string;
    country: string;
}