export class Product {

    id: number;
    name: string;
    unitPrice: number;
    priceMax: number;
    quantity: number;
    img: string;
    idCategorie: number;

    constructor(id: number, name: string, unitPrice: number, priceMax: number, quantity: number, idCategory: number) {
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.priceMax = priceMax;
        this.quantity = quantity;
        this.idCategorie = idCategory

        switch (idCategory) {
            case 1:
                this.img = "./../../../assets/img/cat/alimentaire/" + this.name + ".png"
                break;
            case 2:
                this.img = "./../../../assets/img/cat/hygiene/" + this.name + ".png"
                break;
            case 10:
                this.img = "./../../../assets/img/cat/viande/" + this.name + ".png"
                break;
            case 11:
                this.img = "./../../../assets/img/cat/legume/" + this.name + ".png"
                break;
            case 12:
                this.img = "./../../../assets/img/cat/fruit/" + this.name + ".png"
                break;
            case 13:
                this.img = "./../../../assets/img/cat/fromage/" + this.name + ".png"
                break;
            case 14:
                this.img = "./../../../assets/img/cat/feculent/" + this.name + ".png"
                break;
            case 15:
                this.img = "./../../../assets/img/cat/entretien/" + this.name + ".png"
                break;
        }
    }
}