import { Category } from './Category';
import { User } from './User';

export class Order {

    id: number;
    numero: string;
    owner: User;
    status: number = 0;
    deliveryMan: User;
    categoryOrdered: Category[] = [];
    date: string;
    total: number;

}