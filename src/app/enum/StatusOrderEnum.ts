export enum StatusOrderEnum {
    NotAssigned = 0,
    InProgress = 1,
    Finished = 2
}