import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  /**
   * Variable qui contient l'utilisateur courant (null s'il n'y aucun utilisateur connecté)
  */
  public currentUser: User = new User();

  /**
   * Variable qui permet l'abonnement à l'utilisateur courant !
  */
  public currentUserSubject: Subject<User> = new Subject<User>();

  /**
  * Variable qui contient l'adresse URL des services de gestion d'un USER
 */
  public endpoint_url_public: string = "http://149.91.89.231:8080/public/users/";
  public endpoint_url: string = "http://149.91.89.231:8080/users/";

  constructor(private http: HttpClient, private router: Router) { }

  /**
   * Méthode pour envoyer tous les informations du currentUser dans l'architecture du site
  */
  emitCurrentUserSubject() {
    this.currentUserSubject.next(this.currentUser);
  }

  /**
   * Méthode pour vérifier si l'utilisateur est bien connecté à l'application
  */
  isAuthenticated(): boolean {
    this.tokenTiming();

    if (localStorage.getItem('token') == null) {
      return false;
    }

    return true;
  }

  /**
   * Méthode d'appel à l'API pour créer un USER
  */
  createUser(user: User): void {
    this.http.post(this.endpoint_url_public + "create", JSON.stringify(user)).subscribe((res) => {
    });
  }

  /**
   * Méthode pour se connecter
  */
  getCurrentUser(): void {
    if (this.getToken() != null) {
      this.http.get(this.endpoint_url + "current").subscribe((res: any) => {
        this.currentUser.id = res.userId
        this.currentUser.lastname = res.lastname;
        this.currentUser.firstname = res.firstname;
        this.currentUser.mail = res.mail;

        let datePipe = new DatePipe('en-EN');
        var date = datePipe.transform(res.dateOfBirth, 'MM/dd/yyyy');
        this.currentUser.dateOfBirth = date;

        localStorage.setItem('userId', res.userId);
      });

      this.emitCurrentUserSubject();
    }
  }

  /**
   * Méthode pour se connecter
  */
  login(mail: string, password: string): void {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let login = { mail: mail, password: password }

    this.http.post(this.endpoint_url_public + "login", JSON.stringify(login), httpOptions).subscribe((res: any) => {
      localStorage.setItem('token', res.token);
      localStorage.setItem('email', mail);

      let now = new Date().getTime();
      localStorage.setItem('tokenTiming', now.toString());

      this.router.navigate(['action']);
    })

  }

  /**
   * Méthode pour se déconnecter
  */
  disconnect(): void {
    localStorage.clear();
  }

  /**
   * Méthode pour se connecter
  */
  getToken(): string {
    return localStorage.getItem('token');
  }

  getEmailToken(): string {
    return localStorage.getItem('email');
  }

  tokenTiming() {
    var hours = 2; // Reset when storage is more than 2hours
    var now = new Date().getTime();
    var setupTime = localStorage.getItem('tokenTiming');
    if (setupTime == null) {
      console.log('Error identification');
    } else {
      if (now - (+(setupTime)) > hours * 60 * 60 * 1000) {
        localStorage.clear();
      }
    }
  }

}
