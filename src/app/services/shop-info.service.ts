import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShopInfoService {

  displayShopInfo: boolean = false;

  displayShSubject: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  emitDisplayShSubject() {
    this.displayShSubject.next(this.displayShopInfo);
  }

  display() {
    this.displayShopInfo = true;
    this.emitDisplayShSubject();
  }

  hide() {
    this.displayShopInfo = false;
    this.emitDisplayShSubject();
  }

}
