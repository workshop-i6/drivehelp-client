import { Injectable, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Category } from '../models/Category';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/Product';
import { Order } from '../models/Order';
import { AuthenticationService } from './authentication.service';
import { User } from '../models/User';
import { StatusOrderEnum } from '../enum/StatusOrderEnum';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  endpoint_url_cat: string = "http://149.91.89.231:8080/categories";
  endpoint_url_order: string = "http://149.91.89.231:8080/commandes";
  endpoint_url_order_owner: string = "http://149.91.89.231:8080/commandes/demandeur/";

  categoryList: Category[] = [];
  categoryListSubject: Subject<Category[]> = new Subject<Category[]>();

  order: Order;
  orderSubject: Subject<Order> = new Subject<Order>();

  orderOwner: Order;
  orderOwnerSubject: Subject<any> = new Subject<any>();

  allOrdersNotOwned: Order[];
  allOrdersNotOwnedSubject: Subject<Order[]> = new Subject<Order[]>();

  orderAssigned: Order;
  orderAssignedSubject: Subject<Order> = new Subject<Order>();

  constructor(private http: HttpClient, private authService: AuthenticationService) {
  }

  emitCategoryListSubject(): void {
    this.categoryListSubject.next(this.categoryList);
  }

  emitOrderSubject(): void {
    this.orderSubject.next(this.order);
  }

  emitOrderOwnerSubject(): void {
    this.orderOwnerSubject.next(this.orderOwner);
  }

  emitAllOrdersNotOwnedSubject(): void {
    this.allOrdersNotOwnedSubject.next(this.allOrdersNotOwned);
  }

  emitOrderAssignedSubject(): void {
    this.orderAssignedSubject.next(this.orderAssigned);
  }

  getAllProductWithCategory(): void {
    this.categoryList = [];

    this.http.get(this.endpoint_url_cat).subscribe((res: any) => {
      this.resultWrapper(res);
      this.emitCategoryListSubject();
    });
  }

  resultWrapper(res: any): void {
    res.forEach(categoryRes => {
      let category = new Category(categoryRes.id, categoryRes.name, []);
      categoryRes.produits.forEach(productRes => {
        let product = new Product(productRes.id, productRes.nom, +productRes.prixMax, 0, 0, categoryRes.id);
        category.products.push(product);
      });
      this.categoryList.push(category)
    });
  }

  clearCategoryList() {
    this.categoryList = [];
    this.emitCategoryListSubject();
  }

  clearOrder() {
    this.order.categoryOrdered = [];
    this.emitOrderSubject();
  }

  qantityProduct(product: Product, quantity: number): void {
    this.categoryList.forEach((categoryItem: Category) => {
      if (categoryItem.id == product.idCategorie) {
        categoryItem.products.forEach((productItem: Product) => {
          if (productItem.id == product.id) {
            productItem.quantity = productItem.quantity + quantity;
            productItem.priceMax = productItem.quantity * productItem.unitPrice;
          }
        })
      }
    });

    this.makeOrder();
    this.emitCategoryListSubject();
  }

  createOrder(currentUser: User): void {
    this.order = new Order();
    this.order.owner = currentUser;
  }

  makeOrder(): void {

    this.order.categoryOrdered = [];
    let categoryListWorking = this.categoryList;

    categoryListWorking.forEach((category: Category) => {

      if (this.containsCategoryOrder(category)) {

        let categoryTarget = this.cloneCategory(category);
        categoryTarget.products = []

        category.products.forEach((product: Product) => {
          if (this.containsProductOrder(product)) {
            categoryTarget.products.push(this.cloneProduct(product));
          }
        });

        this.order.categoryOrdered.push(categoryTarget);
      }
    });

    this.emitOrderSubject();
  }

  filterCategory(category: Category): boolean {
    let array = category.products.filter(product => product.quantity > 0);

    if (array.length > 0) {
      return true;
    }

    return false;
  }

  filterProduct(product: Product): boolean {
    if (product.quantity > 0) {
      return true;
    }

    return false;
  }

  cloneCategory(cible: Category): Category {
    let categoryClone = new Category(cible.id, cible.name, cible.products);
    return categoryClone;
  };


  cloneProduct(cible: Product): Product {
    let productClone = new Product(cible.id, cible.name, cible.unitPrice, cible.priceMax, cible.quantity, cible.idCategorie);
    return productClone;
  }

  containsCategoryOrder(category: Category) {
    if (category.products.length > 0) {
      let i = 0;
      while ((i < category.products.length)) {
        if (category.products[i].quantity > 0) {
          return true;
        }

        i++;
      }
    }
    return false;
  }

  containsProductOrder(product: Product) {
    if (product.quantity > 0) {
      return true;
    }

    return false;
  }

  sendOrder() {

    if (this.order != null && this.order.categoryOrdered != []) {

      let lines: any[] = [];

      this.order.categoryOrdered.forEach((category: Category) => {
        category.products.forEach((product: Product) => {
          lines.push({ id: product.id, name: product.name, priceMax: product.unitPrice, quantity: product.quantity } as any)
        });
      })

      let objToSend = {
        "demandeur": this.authService.currentUser.id,
        "lines": lines
      }

      this.http.post(this.endpoint_url_order + "/create", objToSend).subscribe((res: any) => {
        this.getAllOrdersOwner();
      });

      this.order.categoryOrdered = [];

    }

  }

  getAllOrdersOwner() {
    let endpoint_result = this.endpoint_url_order_owner + localStorage.getItem('userId');

    this.http.get(endpoint_result).subscribe((res: any) => {
      if(res.length != 0) {
        this.orderOwner = this.orderOwnerWrapper(res)[0];
      } else {
        this.orderOwner = null;
      }
      
      this.emitOrderOwnerSubject();
    })


  }

  orderOwnerWrapper(res: any): Order[] {

    if (res.length == 0) {
      return null;
    }

    let statusOrderEnum = StatusOrderEnum;

    let categoryListOrderOwner: Category[] = [];
    let orderOwnerList: Order[] = [];

    let indexOfCurrentOrder: number;

    res.forEach((item: any) => {

      let orderOwner: Order = new Order();
      orderOwner.categoryOrdered = [];

      console.log(res);

      if (statusOrderEnum.NotAssigned == +item.status || statusOrderEnum.InProgress == +item.status) {
        item.lines.forEach((line => {
          let cat = line.produit.categorie;
          let prod = line.produit;

          let category = new Category(cat.id, cat.name, []);
          if (!this.containsCategory(categoryListOrderOwner, category)) {
            categoryListOrderOwner.push(category);
          }

          let product = new Product(prod.id, prod.nom, prod.prixMax, 0, line.quantite, cat.id);
          if (!this.containsProductInCategory(categoryListOrderOwner, product)) {
            this.addProductToCategory(categoryListOrderOwner, product);
          }

          indexOfCurrentOrder = res.indexOf(item);
        }));

        orderOwner.categoryOrdered = categoryListOrderOwner;
        orderOwner.id = res[indexOfCurrentOrder].id;
        orderOwner.owner = new User();
        orderOwner.owner.id = res[indexOfCurrentOrder].demandeur.id;
        orderOwner.owner.lastname = res[indexOfCurrentOrder].demandeur.lastname;
        orderOwner.owner.firstname = res[indexOfCurrentOrder].demandeur.firstname;
        orderOwner.total = res[indexOfCurrentOrder].total;
        orderOwner.status = res[indexOfCurrentOrder].status;

        if(res[indexOfCurrentOrder].livreur != null) {
          orderOwner.deliveryMan = new User();
          orderOwner.deliveryMan.id = res[indexOfCurrentOrder].livreur.id;
          orderOwner.deliveryMan.lastname = res[indexOfCurrentOrder].livreur.lastname;
          orderOwner.deliveryMan.firstname = res[indexOfCurrentOrder].livreur.firstname;
        }
        
        orderOwner.numero = res[indexOfCurrentOrder].numero;

        orderOwner.date = res[indexOfCurrentOrder].date.split("-", 3);
        let dd = orderOwner.date[2].split("T", 1);
        orderOwner.date = orderOwner.date[1] + "/" + dd[0] + "/" + orderOwner.date[0];

        orderOwnerList.push(orderOwner);
      }
    });

    return orderOwnerList;
  }

  containsCategory(array: Category[], cible: Category): boolean {

    let result = false;
    array.forEach((cat: Category) => {
      if (cat.id != cible.id) {
        result = false;
      } else {
        result = true
      }
    });
    return result;
  }

  containsProductInCategory(array: Category[], cible: Product): boolean {
    array.forEach((cat: Category) => {
      cat.products.forEach((product: Product) => {
        if (product.id == cible.id) {
          return true;
        }
      });
    });
    return false;
  }

  addProductToCategory(array: Category[], product: Product): void {
    array.find((category: Category) => category.id == product.idCategorie).products.push(product);
  }

  cancelOrder(orderId: number): void {
    this.http.delete(this.endpoint_url_order + "/" + orderId).subscribe((res: any) => {
      this.getAllOrdersOwner();
    })
  }

  getAllOrders() {
    this.http.get(this.endpoint_url_order).subscribe((res: any) => {
      this.allOrdersNotOwned = this.orderOwnerWrapper(res);
      if(this.allOrdersNotOwned.find((order: Order) => this.currentUserIsDeliverMan(order))) {
        this.orderAssigned = this.allOrdersNotOwned.find((order: Order) => this.currentUserIsDeliverMan(order));
        this.emitOrderAssignedSubject();
      }
      if(this.allOrdersNotOwned.find((order: Order) => order.owner.id == +localStorage.getItem('userId'))) {
        this.allOrdersNotOwned.splice(this.allOrdersNotOwned.indexOf(this.allOrdersNotOwned.find((order: Order) => order.owner.id == +localStorage.getItem('userId'))), 1);
      }
      this.emitAllOrdersNotOwnedSubject();
    });
  }

  takeOrderToDeliver(order: Order) {

    let objToSend = {
      "idCommande": order.id,
      "idLivreur": localStorage.getItem('userId')
    }

    this.http.post(this.endpoint_url_order + "/accept", objToSend).subscribe((res: any) => {
      console.log(res);
      this.getAllOrders();
      this.getAllOrdersOwner();
    });
  }

  closeOrder(order: Order) {
    this.http.get(this.endpoint_url_order + "/end/" + order.id).subscribe((res: any) => {
      console.log(res);
      this.orderAssigned = null;
      this.emitOrderAssignedSubject();
      this.getAllOrders();
      this.getAllOrdersOwner();
    })
  }

  currentUserIsDeliverMan(order: Order): boolean {
    if(order.deliveryMan != null) {
      if(order.deliveryMan.id == +localStorage.getItem('userId')) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

}
