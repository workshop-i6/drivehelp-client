#!/bin/bash

TAG_VERSION="$1"
DEPLOYMENT_FILE_PATH="$2"


if [ "$2" == "" ]; then
    echo "Missing parameter! Parameters are TAG_VERSION and DEPLOYMENT_FILE_PATH.";
    exit 1;
fi

cat > ${DEPLOYMENT_FILE_PATH} <<EOL
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: sitecollecte-pod
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: sitecollecte-pod
    spec:
      containers:
      - name: sitecollecte-pod
        image: registry.gitlab.com/mspr-maintenance-et-evolution-de-l-application/site_collecte:${TAG_VERSION}
        imagePullPolicy: Always
        ports: [
          {
            containerPort: 4200
          },
          {
            containerPort: 3232
          }
        ]
      imagePullSecrets:
        - name: registry.gitlab.com
EOL